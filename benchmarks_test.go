package query_test

import (
	. "bitbucket.org/ragnara/query"
	"bitbucket.org/ragnara/query/iter"
	"strconv"
	"testing"
)

type benchData struct {
	number int
	str    string
}

type benchDB []benchData

func createBenchDB(b *testing.B, maxId int) benchDB {
	db := benchDB(make([]benchData, 0, maxId))
	for i := 0; i <= maxId; i++ {
		db = append(db, benchData{i, strconv.Itoa(i)})
	}

	b.ResetTimer()
	return db
}

func (b benchDB) Fields() []Field {
	return []Field{
		{"number", func(i interface{}) (interface{}, bool) {
			return i.(benchData).number, false
		}},
		{"str", func(i interface{}) (interface{}, bool) {
			return i.(benchData).str, false
		}},
	}
}

func (b benchDB) Iter() iter.Iter {
	return iter.NewSliceIter(b)
}

func BenchmarkIntEquals_1(b *testing.B) {
	benchmarkIntEquals(b, 1)
}

func BenchmarkIntEquals_1000(b *testing.B) {
	benchmarkIntEquals(b, 1000)
}

func BenchmarkIntEquals_10000(b *testing.B) {
	benchmarkIntEquals(b, 10000)
}

func BenchmarkIntEquals_100000(b *testing.B) {
	benchmarkIntEquals(b, 100000)
}

func benchmarkIntEquals(b *testing.B, maxId int) {
	b.Helper()
	proc := NewProcessor(createBenchDB(b, maxId))
	query, err := proc.Build("number = 12345")

	if err != nil {
		b.Fatalf("Could not build query: %v", err)
	}

	var results []interface{}
	for i := 0; i < b.N; i++ {
		results = query.Run()
	}

	if maxId < 100000 {
		return
	}

	if len(results) != 1 || results[0].(benchData).number != 12345 {
		b.Fatalf("Unexpected results: %v", results)
	}
}

func BenchmarkIntLessThanAndGreaterThan_1(b *testing.B) {
	benchmarkIntLessThanAndGreaterThan(b, 1)
}

func BenchmarkIntLessThanAndGreaterThan_1000(b *testing.B) {
	benchmarkIntLessThanAndGreaterThan(b, 1000)
}

func BenchmarkIntLessThanAndGreaterThan_10000(b *testing.B) {
	benchmarkIntLessThanAndGreaterThan(b, 10000)
}

func BenchmarkIntLessThanAndGreaterThan_100000(b *testing.B) {
	benchmarkIntLessThanAndGreaterThan(b, 100000)
}

func benchmarkIntLessThanAndGreaterThan(b *testing.B, maxId int) {
	b.Helper()
	proc := NewProcessor(createBenchDB(b, maxId))
	query, err := proc.Build("number > 12339 AND number < 12350")

	if err != nil {
		b.Fatalf("Could not build query: %v", err)
	}

	var results []interface{}
	for i := 0; i < b.N; i++ {
		results = query.Run()
	}

	if maxId < 100000 {
		return
	}

	if len(results) != 10 || results[0].(benchData).number != 12340 || results[9].(benchData).number != 12349 {
		b.Fatalf("Unexpected results: %v", results)
	}
}

func BenchmarkStringEquals_1(b *testing.B) {
	benchmarkStringEquals(b, 1)
}

func BenchmarkStringEquals_1000(b *testing.B) {
	benchmarkStringEquals(b, 1000)
}

func BenchmarkStringEquals_10000(b *testing.B) {
	benchmarkStringEquals(b, 10000)
}

func BenchmarkStringEquals_100000(b *testing.B) {
	benchmarkStringEquals(b, 100000)
}

func benchmarkStringEquals(b *testing.B, maxId int) {
	b.Helper()
	proc := NewProcessor(createBenchDB(b, maxId))
	query, err := proc.Build("str = '345'")

	if err != nil {
		b.Fatalf("Could not build query: %v", err)
	}

	var results []interface{}
	for i := 0; i < b.N; i++ {
		results = query.Run()
	}

	if maxId < 100000 {
		return
	}

	if len(results) != 1 || results[0].(benchData).number != 345 {
		b.Fatalf("Unexpected results: %v", results)
	}
}

func BenchmarkStringLike_1(b *testing.B) {
	benchmarkStringLike(b, 1)
}

func BenchmarkStringLike_1000(b *testing.B) {
	benchmarkStringLike(b, 1000)
}

func BenchmarkStringLike_10000(b *testing.B) {
	benchmarkStringLike(b, 10000)
}

func BenchmarkStringLike_100000(b *testing.B) {
	benchmarkStringLike(b, 100000)
}

func benchmarkStringLike(b *testing.B, maxId int) {
	b.Helper()
	proc := NewProcessor(createBenchDB(b, maxId))
	query, err := proc.Build("str like '1234%'")

	if err != nil {
		b.Fatalf("Could not build query: %v", err)
	}

	var results []interface{}
	for i := 0; i < b.N; i++ {
		results = query.Run()
	}

	if maxId < 100000 {
		return
	}

	if len(results) != 11 || results[0].(benchData).number != 1234 || results[1].(benchData).number != 12340 || results[10].(benchData).number != 12349 {
		b.Fatalf("Unexpected results: %v", results)
	}
}
