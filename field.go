package query

import (
	"bitbucket.org/ragnara/pars/v2"
)

type Field struct {
	Name   string
	Getter func(interface{}) (value interface{}, isNull bool)
}

type parserField struct {
	Field
	q *queryParser
}

func (f parserField) Parsers() []pars.Parser {
	return []pars.Parser{
		identifierParser(f.Name),
		opParser(),
		f.q.valueParser(),
	}
}

func (f parserField) TransformResult(v []interface{}) interface{} {
	op := v[1].(operator)
	compareTo := v[2]
	fn := funcByOP(op, compareTo)
	return func(val interface{}) bool {
		fv := fieldValue{field: Field(f.Field), val: val}
		return fn(fv)
	}
}

func (f parserField) TransformError(err error) error {
	return err
}

type unresolvedValue interface {
	value() interface{}
	isNull() bool
	param() interface{}
}

type fieldValue struct {
	field Field
	val   interface{}
}

var _ unresolvedValue = fieldValue{}

func (f fieldValue) value() interface{} {
	value, _ := f.field.Getter(f.val)
	return value
}

func (f fieldValue) isNull() bool {
	_, isNull := f.field.Getter(f.val)
	return isNull
}

func (f fieldValue) param() interface{} {
	return f.val
}

type constValue struct {
	cval    interface{}
	current interface{}
}

func (c constValue) value() interface{} {
	return c.cval
}

func (c constValue) isNull() bool {
	return c.cval == nil
}

func (c constValue) param() interface{} {
	return c.current
}
