module bitbucket.org/ragnara/query

go 1.13

require (
	bitbucket.org/ragnara/glob v1.0.1
	bitbucket.org/ragnara/pars/v2 v2.1.0
)
