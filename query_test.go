package query_test

import (
	. "bitbucket.org/ragnara/query"
	"bitbucket.org/ragnara/query/fields"
	"bitbucket.org/ragnara/query/iter"
	"fmt"
	"testing"
)

type testdata struct {
	id   int
	id2  int
	name string
}

type testdb []testdata

func (db testdb) Fields() []Field {
	return []Field{
		fields.IntField("id"),
		fields.WithNullValue(fields.IntField("id2"), 0),
		fields.StringField("name"),
	}
}

func (db testdb) Iter() iter.Iter {
	return iter.NewSliceIter(db)
}

func createData() testdb {
	return testdb{
		{id: 1, id2: 6, name: "abc"},
		{id: 2, id2: 5, name: "abc"},
		{id: 3, id2: 4, name: "ABC"},
		{id: 4, id2: 3, name: "def"},
		{id: 5, id2: 2, name: "abcdef"},
		{id: 6, id2: 1, name: "defabc"},
		{id: 7, id2: 0, name: "defabcdef"},
	}
}

type test struct {
	q         string
	resultIdx []int
}

func TestFunctions(t *testing.T) {
	tests := []test{
		{"length(name) >= 6", []int{4, 5, 6}},
		{"not length(name) >= 6", []int{0, 1, 2, 3}},
		{"length(name) = id", []int{2, 5}},
		{"not length(name) = id", []int{0, 1, 3, 4, 6}},
		{"id = length(name)", []int{2, 5}},
		{"id2 = length('abcde')", []int{1}},
		{"mod(id, 2) = 1", []int{0, 2, 4, 6}},
		{"mod(id2, length(name)) = 0", []int{0, 3, 6}},
		{"const_seven() = id", []int{6}},
		{"id = const_seven()", []int{6}},
		{"id = mod(id2, length(name))", []int{1}},
	}

	runTests(t, tests)
}

func TestConstantQueries(t *testing.T) {
	tests := []test{
		{"1 = 1", []int{0, 1, 2, 3, 4, 5, 6}},
		{"0 = 1", []int{}},
		{"null is null", []int{0, 1, 2, 3, 4, 5, 6}},
		{"'abc' = 'def'", []int{}},

		{"not 1 = 1", []int{}},
		{"not 0 = 1", []int{0, 1, 2, 3, 4, 5, 6}},
		{"not null is null", []int{}},
		{"not 'abc' = 'def'", []int{0, 1, 2, 3, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestComplexQueries(t *testing.T) {
	tests := []test{
		{"id = '3' or id = '5'", []int{2, 4}},
		{"id = '5' or name like 'd%'", []int{3, 4, 5, 6}},
		{"id = '3' or id2 = '3'", []int{2, 3}},
		{"id = '3' or id = '5' or id = '1'", []int{0, 2, 4}},
		{"id = '3' and name like 'a%'", []int{2}},
		{"id = '3' and name like 'a%' or id2 = '0'", []int{2, 6}},
		{"id = '3' or name like 'a%' and id2 = '0'", []int{2}},
		{"name like '%a%' AND name like '%d%'", []int{4, 5, 6}},
		{"not name like '%a%' AND name like '%d%'", []int{3}},
		{"not name like '%a%' AND not name like '%d%'", []int{}},
		{"name like '%a%' AND not name like '%d%'", []int{0, 1, 2}},
		{"name like '%d%' and name Like '%ef' OR not id > '1'", []int{0, 3, 4, 6}},
		{"id2 is not null and name like '%d%'", []int{3, 4, 5}},
		{"(id = '3' and name like 'a%') or id2 = '0'", []int{2, 6}},
		{"id = '3' and (name like 'a%' or id2 = '0')", []int{2}},
	}

	runTests(t, tests)
}

func TestQueryWithLikeInt(t *testing.T) {
	tests := []test{
		{"id like '3'", []int{2}},
		{"id like '%3'", []int{2}},
		{"id like '3%'", []int{2}},
		{"id like '%3%'", []int{2}},
		{"id like 3", []int{2}},
	}

	runTests(t, tests)
}

func TestQueryWithLikeString(t *testing.T) {
	tests := []test{
		{"name like '%abc'", []int{0, 1, 2, 5}},
		{"name LIKE 'abc%'", []int{0, 1, 2, 4}},
		{"name like '%abc%'", []int{0, 1, 2, 4, 5, 6}},
		{"name LiKe '%'", []int{0, 1, 2, 3, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithEqualsInt(t *testing.T) {
	tests := []test{
		{"id = '3'", []int{2}},
		{"id = '5'", []int{4}},
		{"id2 = '3'", []int{3}},
		{"id2 = '5'", []int{1}},
		{"id is '3'", []int{2}},
		{"id iS '5'", []int{4}},
		{"id2 is '3'", []int{3}},
		{"id2 is '5'", []int{1}},
		{"id = id2", []int{}},
		{"id = id", []int{0, 1, 2, 3, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithEqualsString(t *testing.T) {
	tests := []test{
		{"name = 'abc'", []int{0, 1, 2}},
		{"name = 'def'", []int{3}},
		{"name IS 'abc'", []int{0, 1, 2}},
		{"name Is 'def'", []int{3}},
	}

	runTests(t, tests)
}

func TestQueryWithLessThanInt(t *testing.T) {
	tests := []test{
		{"id < '3'", []int{0, 1}},
		{"id < '5'", []int{0, 1, 2, 3}},
		{"id2 < '3'", []int{4, 5, 6}},
		{"id2 < '5'", []int{2, 3, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithLessThanString(t *testing.T) {
	tests := []test{
		{"name < 'abc'", []int{}},
		{"name < 'def'", []int{0, 1, 2, 4}},
		{"name < 'abd'", []int{0, 1, 2, 4}},
	}

	runTests(t, tests)
}

func TestQueryWithLessThanOrEqualsInt(t *testing.T) {
	tests := []test{
		{"id <= '3'", []int{0, 1, 2}},
		{"id <= '5'", []int{0, 1, 2, 3, 4}},
		{"id2 <= '3'", []int{3, 4, 5, 6}},
		{"id2 <= '5'", []int{1, 2, 3, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithLessThanOrEqualsString(t *testing.T) {
	tests := []test{
		{"name <= 'abc'", []int{0, 1, 2}},
		{"name <= 'def'", []int{0, 1, 2, 3, 4}},
		{"name <= 'abd'", []int{0, 1, 2, 4}},
	}

	runTests(t, tests)
}

func TestQueryWithGreaterThanInt(t *testing.T) {
	tests := []test{
		{"id > '3'", []int{3, 4, 5, 6}},
		{"id > '5'", []int{5, 6}},
		{"id2 > '3'", []int{0, 1, 2}},
		{"id2 > '5'", []int{0}},
		{"id > id2", []int{3, 4, 5, 6}},
		{"id2 > id", []int{0, 1, 2}},
		{"id > id", []int{}},
	}

	runTests(t, tests)
}

func TestQueryWithGreaterThanString(t *testing.T) {
	tests := []test{
		{"name > 'abc'", []int{3, 4, 5, 6}},
		{"name > 'def'", []int{5, 6}},
		{"name > 'abd'", []int{3, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithGreaterThanOrEqualsInt(t *testing.T) {
	tests := []test{
		{"id >= '3'", []int{2, 3, 4, 5, 6}},
		{"id >= '5'", []int{4, 5, 6}},
		{"id2 >= '3'", []int{0, 1, 2, 3}},
		{"id2 >= '5'", []int{0, 1}},
	}

	runTests(t, tests)
}

func TestQueryWithGreaterThanOrEqualsString(t *testing.T) {
	tests := []test{
		{"name >= 'abc'", []int{0, 1, 2, 3, 4, 5, 6}},
		{"name >= 'def'", []int{3, 5, 6}},
		{"name >= 'abd'", []int{3, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryWithNotEqualsInt(t *testing.T) {
	tests := []test{
		{"id <> '3'", []int{0, 1, 3, 4, 5, 6}},
		{"id <> '5'", []int{0, 1, 2, 3, 5, 6}},
		{"id2 <> '3'", []int{0, 1, 2, 4, 5, 6}},
		{"id2 <> '5'", []int{0, 2, 3, 4, 5, 6}},
		{"id is not '3'", []int{0, 1, 3, 4, 5, 6}},
		{"id is not '5'", []int{0, 1, 2, 3, 5, 6}},
		{"id2 IS nOt '3'", []int{0, 1, 2, 4, 5, 6}},
		{"id2 iS NOT '5'", []int{0, 2, 3, 4, 5, 6}},
		{"id <> id2", []int{0, 1, 2, 3, 4, 5, 6}},
		{"id is not id2", []int{0, 1, 2, 3, 4, 5, 6}},
		{"id <> id", []int{}},
	}

	runTests(t, tests)
}

func TestQueryWithNotEqualsString(t *testing.T) {
	tests := []test{
		{"name <> 'abc'", []int{3, 4, 5, 6}},
		{"name <> 'def'", []int{0, 1, 2, 4, 5, 6}},
		{"name iS Not 'abc'", []int{3, 4, 5, 6}},
		{"name is not 'def'", []int{0, 1, 2, 4, 5, 6}},
	}

	runTests(t, tests)
}

func TestQueryIsNull(t *testing.T) {
	tests := []test{
		{"id2 is null", []int{6}},
		{"id2 is not null", []int{0, 1, 2, 3, 4, 5}},
		{"not id2 is null", []int{0, 1, 2, 3, 4, 5}},
		{"not id2 is not null", []int{6}},
		{"id is null", []int{}},
		{"name is null", []int{}},
	}

	runTests(t, tests)
}

func TestParserErrors(t *testing.T) {
	tests := []struct {
		q   string
		err string
	}{
		{"", "Identifier expected"},
		{"id something...", "Operator expected"},
		{"id = '3'garbage", "Expected EOF: Found byte 0x67"},
		{"id = '3", "String value expected: Could not parse expected rune ''' (0x27): EOF"},
		{"id =", "Value expected"},
		{"id = garbage", "Value expected"},
		{"(id = '3'", "Subexpression expected: Could not parse expected rune ')' (0x29): EOF"},
		{"id = '3' and", "AND Expression expected: Identifier expected"},
		{"id = '3' or", "OR Expression expected: Identifier expected"},
		{"or", "Identifier expected"},
		{"and", "Identifier expected"},
		{"not", "Identifier expected"},
		{"(", "Subexpression expected: Identifier expected"},
		{"id = length", "Value expected"},
		{"id = length(", "Function call to length expected: Could not find expected sequence item 0: Value expected"},
		{"id = length(name", "Function call to length expected: Could not parse expected rune ')' (0x29): EOF"},
		{"id = length(name, )", "Function call to length expected: Could not parse expected rune ')' (0x29): Unexpected rune ',' (0x2c)"},
		{"id = length(name, 2)", "Function call to length expected: Wrong argument count: Expected 1, got 2"},
		{"id = const_seven(1,2,3)", "Function call to const_seven expected: Could not parse expected rune ')' (0x29): Unexpected rune '1' (0x31)"},
	}

	db := createData()
	proc := NewProcessor(db)
	registerTestFns(&proc)
	for _, test := range tests {
		_, err := proc.Build(test.q)
		if err.Error() != test.err {
			t.Errorf("Query {%v}: Expected error %v, but got %v", test.q, test.err, err.Error())
		}
	}
}

func TestAbortStream(t *testing.T) {
	db := createData()
	proc := NewProcessor(db)
	query, _ := proc.Build("id is not null")

	called := 0
	query.Stream(func(val interface{}) bool {
		called++
		if val != db[0] {
			t.Errorf("Got result %#v, but expected %#v", val, db[0])
		}
		return false
	})

	if called != 1 {
		t.Errorf("Expected exactly 1 call, but got %v", called)
	}
}

func TestRunWithLimit(t *testing.T) {
	db := createData()
	proc := NewProcessor(db)

	test := test{"id is not null", []int{0, 1, 2, 3, 4, 5, 6}}
	query, _ := proc.Build(test.q)

	allResults := query.Run()
	firstThree := query.RunWithLimit(3)

	assertResults(t, allResults, db, test)
	assertResultsRaw(t, firstThree, allResults[0:3], "with limit 3")
}

func TestRunWithUnreachedLimit(t *testing.T) {
	db := createData()
	proc := NewProcessor(db)

	test := test{"id is not null", []int{0, 1, 2, 3, 4, 5, 6}}
	query, _ := proc.Build(test.q)

	allResults := query.Run()
	firstThree := query.RunWithLimit(20)

	assertResults(t, allResults, db, test)
	assertResultsRaw(t, firstThree, allResults, "with unreached limit 20")
}

func runTests(t *testing.T, tests []test) {
	t.Helper()
	db := createData()
	proc := NewProcessor(db)
	registerTestFns(&proc)

	for _, test := range tests {
		query, err := proc.Build(test.q)

		if err != nil {
			t.Errorf("Error while building query {%v}: %v", test.q, err)
			continue
		}

		results := query.Run()
		assertResults(t, results, db, test)
	}
}

func registerTestFns(proc *Processor) {
	proc.RegisterFn("length", 1, func(vals []interface{}) interface{} {
		return len(vals[0].(string))
	})

	proc.RegisterFn("mod", 2, func(vals []interface{}) interface{} {
		a := vals[0].(int64)
		b := vals[1].(int64)
		return a % b
	})

	proc.RegisterFn("const_seven", 0, func(vals []interface{}) interface{} {
		return int64(7)
	})
}

func assertResults(t *testing.T, results []interface{}, db testdb, test test) {
	t.Helper()
	expected := getExpected(db, test)

	info := fmt.Sprintf("for query {%v}", test.q)
	assertResultsRaw(t, results, expected, info)
}

func assertResultsRaw(t *testing.T, results []interface{}, expected []interface{}, info string) {
	if len(results) != len(expected) {
		t.Errorf("Expected %#v, but got %#v (%v)", expected, results, info)
		return
	}

	for i, result := range results {
		if result != expected[i] {
			t.Errorf("Expected %#v, but got %#v (%v)", expected[i], result, info)
		}
	}
}

func getExpected(db testdb, test test) []interface{} {
	expected := []interface{}{}
	for _, idx := range test.resultIdx {
		expected = append(expected, db[idx])
	}
	return expected
}
