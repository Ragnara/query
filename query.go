package query

type Query struct {
	matcher func(interface{}) bool
	db      Queryable
}

func (q Query) Run() []interface{} {
	results := []interface{}{}
	q.Stream(func(val interface{}) bool {
		results = append(results, val)
		return true
	})
	return results
}

func (q Query) Stream(f StreamFunc) {
	iter := q.db.Iter()
	for !iter.IsEmpty() {
		front := iter.Peek()
		if q.matcher(front) {
			cont := f(front)
			if !cont {
				return
			}
		}
		iter.Next()
	}
}

func (q Query) RunWithLimit(n int) []interface{} {
	results := make([]interface{}, 0, n)
	count := 0
	q.Stream(func(val interface{}) bool {
		results = append(results, val)
		count++
		return count < n
	})
	return results
}

type StreamFunc = func(interface{}) bool

type queryFunc = func(interface{}) bool
