package query

import (
	"bitbucket.org/ragnara/query/iter"
)

type Processor struct {
	db        Queryable
	fields    []Field
	functions []fnData
}

func NewProcessor(db Queryable) Processor {
	fields := db.Fields()
	return Processor{db: db, fields: fields}
}

func (p *Processor) RegisterFn(name string, arity int, fn Fn) {
	p.functions = append(p.functions, fnData{
		name:  name,
		arity: arity,
		f:     fn})
}

func (p *Processor) Build(query string) (*Query, error) {
	qp := queryParser{fields: p.fields, functions: p.functions}
	matcher, err := qp.parseQuery(query)

	if err != nil {
		return nil, err
	}

	return &Query{db: p.db, matcher: matcher}, nil
}

type Queryable interface {
	Fields() []Field
	Iter() iter.Iter
}
