package query

import (
	"testing"
)

func TestOperators(t *testing.T) {
	tests := []struct {
		left, right interface{}
		op          operator
		expected    bool
	}{
		{2, 10, opLessThan, true},
		{"2", 10, opLessThan, true},
		{2, "10", opLessThan, true},
		{"2", "10", opLessThan, false},
		{"2", "2a", opEquals, false},
		{2, "2a", opEquals, true},
		{"2a", "2a", opEquals, true},
		{"2a", 2, opEquals, true},
		{"2a", "2", opEquals, false},
		{2, 2, opEquals, true},
		{5, 4, opLike, false},
		{5, "4", opLike, false},
		{"5", 4, opLike, false},
		{"5", "4", opLike, false},
		{"5", "%4%", opLike, false},
		{"355", "%4%", opLike, false},
		{4, 4, opLike, true},
		{4, "4", opLike, true},
		{"4", 4, opLike, true},
		{"4", "4", opLike, true},
		{"4", "%4%", opLike, true},
		{"345", "%4%", opLike, true},

		{"0", nil, opLessThan, false},
		{0, nil, opLessThan, false},
		{"", nil, opLessThan, false},
		{"ThisIsNull", nil, opLessThan, false},
		{"0", nil, opEquals, false},
		{0, nil, opEquals, false},
		{"", nil, opEquals, false},
		{"ThisIsNull", nil, opEquals, true},
		{"0", nil, opNotEquals, true},
		{0, nil, opNotEquals, true},
		{"", nil, opNotEquals, true},
		{"ThisIsNull", nil, opNotEquals, false},
		{"0", nil, opLike, false},
		{0, nil, opLike, false},
		{"", nil, opLike, false},
		{"ThisIsNull", nil, opLike, false},
		{"0", nil, opGreaterThan, false},
		{0, nil, opGreaterThan, false},
		{"", nil, opGreaterThan, false},
		{"ThisIsNull", nil, opGreaterThan, false},
		{"0", nil, opLessThanOrEquals, false},
		{0, nil, opLessThanOrEquals, false},
		{"", nil, opLessThanOrEquals, false},
		{"ThisIsNull", nil, opLessThanOrEquals, false},
		{"0", nil, opGreaterThanOrEquals, false},
		{0, nil, opGreaterThanOrEquals, false},
		{"", nil, opGreaterThanOrEquals, false},
		{"ThisIsNull", nil, opGreaterThanOrEquals, false},
	}

	for i, test := range tests {
		idField := Field{Name: "",
			Getter: func(i interface{}) (interface{}, bool) {
				isNull := false
				if iStr, iOk := i.(string); iOk {
					isNull = iStr == "ThisIsNull"
				}
				return i, isNull
			}}
		leftFV := fieldValue{field: idField, val: test.left}

		result := funcByOP(test.op, test.right)(leftFV)
		if result != test.expected {
			if result {
				t.Errorf("Test %d (%#v) succeeded, but should have failed", i, test)
			} else {
				t.Errorf("Test %d (%#v) failed, but should have succeeded", i, test)
			}
		}
	}
}
