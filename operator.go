package query

import (
	"bitbucket.org/ragnara/glob"
	"fmt"
	"math/bits"
	"strconv"
	"strings"
)

type rawMatcherFunc = func(unresolvedValue, interface{}) bool
type matcherFunc = func(unresolvedValue) bool

type operator int

const (
	opError operator = iota
	opEquals
	opNotEquals
	opLessThanOrEquals
	opGreaterThanOrEquals
	opLessThan
	opGreaterThan
	opLike
)

func funcByOP(op operator, operand interface{}) matcherFunc {
	operand = upgradeIntType(operand)
	switch op {
	case opEquals:
		return passOperand(operand, matchEquals)
	case opNotEquals:
		return passOperand(operand, not(matchEquals))
	case opLessThanOrEquals:
		return passOperand(operand, failOnNil(not(matchGreaterThan)))
	case opGreaterThanOrEquals:
		return passOperand(operand, failOnNil(not(matchLessThan)))
	case opLessThan:
		return passOperand(operand, failOnNil(matchLessThan))
	case opGreaterThan:
		return passOperand(operand, failOnNil(matchGreaterThan))
	case opLike:
		if bStr, bOk := operand.(string); bOk {
			pattern := strings.ToLower(bStr)
			globber := glob.New(pattern, glob.SQLLike)
			return func(uv unresolvedValue) bool {
				return matchLikeGlob(uv, globber)
			}
		}
		return passOperand(operand, failOnNil(matchLikeNoGlob))
	}
	panic("Unknown Operator")
}

func passOperand(operand interface{}, fn rawMatcherFunc) matcherFunc {
	return func(uv unresolvedValue) bool {
		return fn(uv, resolveArgument(operand, uv.param()))
	}
}

func failOnNil(fn rawMatcherFunc) rawMatcherFunc {
	return func(uv unresolvedValue, b interface{}) bool {
		if b == nil {
			return false
		}
		return fn(uv, b)
	}
}

func not(fn rawMatcherFunc) rawMatcherFunc {
	return func(uv unresolvedValue, b interface{}) bool {
		return !fn(uv, b)
	}
}

func compareWithSameType(a interface{}, b interface{}, onInt func(int64, int64) bool, onStr func(string, string) bool) bool {
	if aStr, aOk := a.(string); aOk {
		if bStr, bOk := b.(string); bOk {
			return onStr(aStr, bStr)
		}

		var aAsInt int64
		fmt.Sscanf(aStr, "%d", &aAsInt)

		return onInt(aAsInt, b.(int64))
	}

	if bStr, bOk := b.(string); bOk {
		var bAsInt int64
		fmt.Sscanf(bStr, "%d", &bAsInt)
		return onInt(a.(int64), bAsInt)
	}

	return onInt(a.(int64), b.(int64))
}

func matchEquals(uv unresolvedValue, b interface{}) bool {
	if b == nil {
		return uv.isNull()
	}

	a := getUnresolvedValue(uv)
	return compareWithSameType(a, b, func(a, b int64) bool { return a == b }, func(a, b string) bool { return strings.EqualFold(a, b) })
}

func matchLessThan(uv unresolvedValue, b interface{}) bool {
	a := getUnresolvedValue(uv)
	return compareWithSameType(a, b, func(a, b int64) bool { return a < b }, func(a, b string) bool { return strings.ToLower(a) < strings.ToLower(b) })
}

func matchGreaterThan(uv unresolvedValue, b interface{}) bool {
	a := getUnresolvedValue(uv)
	return compareWithSameType(a, b, func(a, b int64) bool { return a > b }, func(a, b string) bool { return strings.ToLower(a) > strings.ToLower(b) })
}

func matchLikeNoGlob(uv unresolvedValue, b interface{}) bool {
	a := getUnresolvedValue(uv)
	var aStr string
	if _, ok := a.(int64); ok {
		if _, ok := b.(int64); ok {
			return a.(int64) == b.(int64)
		}
		aStr = strconv.FormatInt(a.(int64), 10)
	} else {
		aStr = a.(string)
	}

	bStr := strconv.FormatInt(b.(int64), 10)

	result := aStr == bStr
	return result
}

func matchLikeGlob(uv unresolvedValue, b glob.Globber) bool {
	a := getUnresolvedValue(uv)
	var aStr string
	if aInt, ok := a.(int64); ok {
		aStr = strconv.FormatInt(aInt, 10)
	} else {
		aStr = a.(string)
	}

	aStr = strings.ToLower(aStr)
	return b.Glob(aStr)
}

func getUnresolvedValue(uv unresolvedValue) interface{} {
	return upgradeIntType(uv.value())
}

func upgradeIntType(v interface{}) interface{} {
	switch v.(type) {
	case int:
		return int64(v.(int))
	case int8:
		return int64(v.(int8))
	case int16:
		return int64(v.(int16))
	case int32:
		return int64(v.(int32))
	case uint8:
		return int64(v.(uint8))
	case uint16:
		return int64(v.(uint16))
	case uint32:
		return int64(v.(uint32))
	case uint64:
		panic("field types of uint64 are not supported yet")
	case uint:
		if bits.UintSize == 64 {
			panic("field types of uint with 64 bit size are not supported yet")
		}
		return int64(v.(uint))
	default:
		return v
	}
}

func resolveArgument(operand interface{}, val interface{}) interface{} {
	switch typedOp := operand.(type) {
	case parsedFn:
		args := make([]interface{}, 0, len(typedOp.params))
		for _, param := range typedOp.params {
			args = append(args, resolveArgument(upgradeIntType(param), val))
		}

		result := upgradeIntType(typedOp.f(args))
		return result
	case Field:
		operandUv := fieldValue{field: typedOp, val: val}
		return getUnresolvedValue(operandUv)
	default:
		return operand
	}
}
