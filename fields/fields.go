package fields

import (
	"bitbucket.org/ragnara/query"
	"math/bits"
	"reflect"
)

func StringField(name string) query.Field {
	getter := func(entry interface{}) (interface{}, bool) {
		val := reflect.ValueOf(entry).FieldByName(name).String()
		return val, false
	}
	return query.Field{Name: name, Getter: getter}
}

func IntField(name string) query.Field {
	getter := func(entry interface{}) (interface{}, bool) {
		field := reflect.ValueOf(entry).FieldByName(name)
		kind := field.Kind()

		var val int64

		if kind >= reflect.Int && kind <= reflect.Int64 {
			val = field.Int()
		} else if (kind > reflect.Uint && kind <= reflect.Uint32) ||
			(kind == reflect.Uint && bits.UintSize == 32) {
			val = int64(field.Uint())
		} else {
			panic("Unsupported field type")
		}

		return val, false
	}
	return query.Field{Name: name, Getter: getter}
}

func StringPtrField(name string) query.Field {
	getter := func(entry interface{}) (interface{}, bool) {
		field := reflect.ValueOf(entry).FieldByName(name)
		if field.IsNil() {
			return "", true
		}

		return field.Elem().String(), false
	}
	return query.Field{Name: name, Getter: getter}
}

func IntPtrField(name string) query.Field {
	getter := func(entry interface{}) (interface{}, bool) {
		field := reflect.ValueOf(entry).FieldByName(name)
		if field.IsNil() {
			return int64(0), true
		}

		var val int64

		elemKind := field.Type().Elem().Kind()
		if elemKind >= reflect.Int && elemKind <= reflect.Int64 {
			val = field.Elem().Int()
		} else if (elemKind > reflect.Uint && elemKind <= reflect.Uint32) ||
			(elemKind == reflect.Uint && bits.UintSize == 32) {
			val = int64(field.Elem().Uint())
		} else {
			panic("unsupported pointer type")
		}

		return val, false
	}
	return query.Field{Name: name, Getter: getter}
}

func WithNullValue(field query.Field, nullValue interface{}) query.Field {
	nullValue = upgradeIntType(nullValue)
	getter := func(entry interface{}) (interface{}, bool) {
		val, isNull := field.Getter(entry)
		if isNull {
			return val, isNull
		}

		if val == nullValue {
			return val, true
		}

		return val, false
	}

	return query.Field{Name: field.Name, Getter: getter}
}

func WithFieldName(field query.Field, name string) query.Field {
	return query.Field{Name: name, Getter: field.Getter}
}

func upgradeIntType(v interface{}) interface{} {
	switch v.(type) {
	case int:
		return int64(v.(int))
	case int8:
		return int64(v.(int8))
	case int16:
		return int64(v.(int16))
	case int32:
		return int64(v.(int32))
	case uint8:
		return int64(v.(uint8))
	case uint16:
		return int64(v.(uint16))
	case uint32:
		return int64(v.(uint32))
	case uint64:
		panic("field types of uint64 are not supported yet")
	case uint:
		if bits.UintSize == 64 {
			panic("field types of uint with 64 bit size are not supported yet")
		}
		return int64(v.(uint))
	default:
		return v
	}
}
