package fields

import (
	"bitbucket.org/ragnara/query"
	"testing"
)

type testStruct struct {
	i      int
	s      string
	iptr   *int
	sptr   *string
	ui8    uint8
	ui8ptr *uint8
}

var testdata = createTestdata()

func createTestdata() []testStruct {
	iptr := new(int)
	*iptr = 3
	sptr := new(string)
	*sptr = "4"
	ui8ptr := new(uint8)
	*ui8ptr = 8

	return []testStruct{
		{i: 1, s: "2", iptr: iptr, sptr: sptr, ui8: 7, ui8ptr: ui8ptr},
		{i: 5, s: "6", iptr: nil, sptr: nil, ui8: 9, ui8ptr: nil},
	}
}

func TestStringField(t *testing.T) {
	f := StringField("s")
	assertString(t, "2", value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertString(t, "6", value(f, testdata[1]))
	assertIsNull(t, false, isNull(f, testdata[1]))
}

func TestStringPtrField(t *testing.T) {
	f := StringPtrField("sptr")
	assertString(t, "4", value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertString(t, "", value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))
}

func TestIntField(t *testing.T) {
	f := IntField("i")
	assertInt(t, 1, value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertInt(t, 5, value(f, testdata[1]))
	assertIsNull(t, false, isNull(f, testdata[1]))

	f2 := IntField("ui8")
	assertInt(t, 7, value(f2, testdata[0]))
	assertIsNull(t, false, isNull(f2, testdata[0]))
	assertInt(t, 9, value(f2, testdata[1]))
	assertIsNull(t, false, isNull(f2, testdata[1]))
}

func TestIntPtrField(t *testing.T) {
	f := IntPtrField("iptr")
	assertInt(t, 3, value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertInt(t, 0, value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))

	f2 := IntPtrField("ui8ptr")
	assertInt(t, 8, value(f2, testdata[0]))
	assertIsNull(t, false, isNull(f2, testdata[0]))
	assertInt(t, 0, value(f2, testdata[1]))
	assertIsNull(t, true, isNull(f2, testdata[1]))
}

func TestWithNullValueToStringField(t *testing.T) {
	f := WithNullValue(StringField("s"), "6")
	assertString(t, "2", value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertString(t, "6", value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))
}

func TestWithNullValueToStringPtrField(t *testing.T) {
	f := WithNullValue(StringPtrField("sptr"), "4")
	assertString(t, "4", value(f, testdata[0]))
	assertIsNull(t, true, isNull(f, testdata[0]))
	assertString(t, "", value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))

	f2 := WithNullValue(StringPtrField("sptr"), "something else")
	assertString(t, "4", value(f2, testdata[0]))
	assertIsNull(t, false, isNull(f2, testdata[0]))
	assertString(t, "", value(f2, testdata[1]))
	assertIsNull(t, true, isNull(f2, testdata[1]))
}

func TestWithNullValueToIntField(t *testing.T) {
	f := WithNullValue(IntField("i"), 5)
	assertInt(t, 1, value(f, testdata[0]))
	assertIsNull(t, false, isNull(f, testdata[0]))
	assertInt(t, 5, value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))

	f2 := WithNullValue(IntField("ui8"), 9)
	assertInt(t, 7, value(f2, testdata[0]))
	assertIsNull(t, false, isNull(f2, testdata[0]))
	assertInt(t, 9, value(f2, testdata[1]))
	assertIsNull(t, true, isNull(f2, testdata[1]))
}

func TestWithNullValueToIntPtrField(t *testing.T) {
	f := WithNullValue(IntPtrField("iptr"), 3)
	assertInt(t, 3, value(f, testdata[0]))
	assertIsNull(t, true, isNull(f, testdata[0]))
	assertInt(t, 0, value(f, testdata[1]))
	assertIsNull(t, true, isNull(f, testdata[1]))

	f2 := WithNullValue(IntPtrField("ui8ptr"), 8)
	assertInt(t, 8, value(f2, testdata[0]))
	assertIsNull(t, true, isNull(f2, testdata[0]))
	assertInt(t, 0, value(f2, testdata[1]))
	assertIsNull(t, true, isNull(f2, testdata[1]))

	f3 := WithNullValue(IntPtrField("iptr"), 12345)
	assertInt(t, 3, value(f3, testdata[0]))
	assertIsNull(t, false, isNull(f3, testdata[0]))
	assertInt(t, 0, value(f3, testdata[1]))
	assertIsNull(t, true, isNull(f3, testdata[1]))

	f4 := WithNullValue(IntPtrField("ui8ptr"), 12345)
	assertInt(t, 8, value(f4, testdata[0]))
	assertIsNull(t, false, isNull(f4, testdata[0]))
	assertInt(t, 0, value(f4, testdata[1]))
	assertIsNull(t, true, isNull(f4, testdata[1]))
}

func TestFieldName(t *testing.T) {
	f := IntField("i")
	renamed := WithFieldName(f, "Changed")
	assertInt(t, 1, value(renamed, testdata[0]))
	assertIsNull(t, false, isNull(renamed, testdata[0]))
	assertInt(t, 5, value(renamed, testdata[1]))
	assertIsNull(t, false, isNull(renamed, testdata[1]))

	assertFieldName(t, f, "i")
	assertFieldName(t, renamed, "Changed")
	assertFieldName(t, WithNullValue(f, "..."), "i")
	assertFieldName(t, WithNullValue(renamed, "..."), "Changed")
}

func assertString(t *testing.T, expected string, actual interface{}) {
	t.Helper()

	if actualStr, ok := actual.(string); ok {
		if actualStr != expected {
			t.Errorf("Expected '%v', but got '%v'", expected, actualStr)
		}
		return
	}
	t.Errorf("Expected string '%v', but got '%#v'", expected, actual)
}

func assertInt(t *testing.T, expected int64, actual interface{}) {
	t.Helper()

	if actualInt, ok := actual.(int64); ok {
		if actualInt != expected {
			t.Errorf("Expected '%v', but got '%v'", expected, actualInt)
		}
		return
	}
	t.Errorf("Expected int64 '%v', but got '%#v'", expected, actual)
}

func assertIsNull(t *testing.T, expected bool, actual bool) {
	t.Helper()

	if expected != actual {
		t.Errorf("IsNull: Expected %v, got %v", expected, actual)
	}
}

func assertFieldName(t *testing.T, f query.Field, expected string) {
	if f.Name != expected {
		t.Errorf("Expected field name '%v', but it was '%v'", expected, f.Name)
	}
}

func value(f query.Field, entry interface{}) interface{} {
	val, _ := f.Getter(entry)
	return val
}

func isNull(f query.Field, entry interface{}) bool {
	_, isNull := f.Getter(entry)
	return isNull
}
