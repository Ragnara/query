package query

import (
	"bitbucket.org/ragnara/pars/v2"
	"fmt"
)

type Fn = func([]interface{}) interface{}

type fnData struct {
	name  string
	f     Fn
	arity int
}

type parsedFn struct {
	f      Fn
	params []interface{}
}

type fnParser struct {
	fnData
	q *queryParser
}

func (f fnParser) Parsers() []pars.Parser {
	parsers := make([]pars.Parser, 0, 3)

	//"name("
	parsers = append(parsers,
		pars.DiscardRight(
			identifierParser(f.name),
			pars.Char('(')))

	//arguments
	if f.arity > 0 {
		parsers = append(parsers,
			pars.Transformer(
				pars.Sep(pars.Recursive(f.q.valueParser),
					pars.Char(',')),
				func(val interface{}) (interface{}, error) {
					vals := val.([]interface{})
					if len(vals) != f.arity {
						return nil,
							fmt.Errorf("Wrong argument count: Expected %v, got %v", f.arity, len(vals))
					}
					return val, nil
				}))
	}

	parsers = append(parsers, pars.Char(')'))

	return parsers
}

func (f fnParser) TransformResult(v []interface{}) interface{} {
	var params []interface{} = nil
	if f.arity > 0 {
		params = v[1].([]interface{})
	}

	return parsedFn{
		f:      f.f,
		params: params,
	}
}

func (f fnParser) TransformError(err error) error {
	return err
}
