package query

import (
	"bitbucket.org/ragnara/pars/v2"
	"errors"
	"unicode"
)

type queryParser struct {
	fields    []Field
	functions []fnData
}

func (q *queryParser) parseQuery(query string) (queryFunc, error) {
	matcher, err := pars.ParseString(query, pars.DiscardRight(q.parser(), pars.EOF))
	if err != nil {
		return nil, err
	}

	return matcher.(queryFunc), nil
}

func (q *queryParser) parser() pars.Parser {
	return pars.Dispatch(
		q.newOrClause(),
		q.newAndClause(),
		q.termClause())
}

func (q *queryParser) newSubExpressionClause() pars.DispatchClause {
	return pars.DescribeClause{
		DispatchClause: subexpressionClause{q},
		Description:    "Subexpression"}
}

func (q *queryParser) newOrClause() pars.DispatchClause {
	return pars.DescribeClause{
		DispatchClause: orClause{q},
		Description:    "OR Expression"}
}

func (q *queryParser) newAndClause() pars.DispatchClause {
	return pars.DescribeClause{
		DispatchClause: andClause{q},
		Description:    "AND Expression"}
}

func (q *queryParser) termClause() pars.DispatchClause {
	return pars.DescribeClause{
		DispatchClause: pars.Clause{q.fieldParser()},
		Description:    "query term"}
}

func (q *queryParser) fieldParser() pars.Parser {
	clauses := make([]pars.DispatchClause, 0, len(q.fields)*2+4)

	clauses = append(clauses, q.newSubExpressionClause())

	for i := range q.fields {
		clauses = append(clauses, negated{parserField{q.fields[i], q}}, parserField{q.fields[i], q})
	}

	clauses = append(clauses, negated{lhsExpression{q}}, lhsExpression{q})

	clauses = append(clauses, pars.Clause{pars.Error(errors.New("Identifier expected"))})
	return pars.Dispatch(clauses...)
}

type subexpressionClause struct {
	q *queryParser
}

func (b subexpressionClause) Parsers() []pars.Parser {
	return []pars.Parser{
		pars.SwallowWhitespace(pars.Char('(')),
		pars.Recursive(b.q.parser),
		pars.SwallowWhitespace(pars.Char(')')),
	}
}

func (b subexpressionClause) TransformResult(r []interface{}) interface{} {
	return r[1].(queryFunc)
}

func (b subexpressionClause) TransformError(err error) error {
	return err
}

type orClause struct {
	q *queryParser
}

func (o orClause) Parsers() []pars.Parser {
	return []pars.Parser{
		pars.DiscardRight(
			pars.Dispatch(
				o.q.newAndClause(),
				o.q.termClause()),
			wholeWordParser(pars.StringCI("or"))),
		pars.Recursive(o.q.parser),
	}
}

func (o orClause) TransformResult(fns []interface{}) interface{} {
	left := fns[0].(queryFunc)
	right := fns[1].(queryFunc)
	return func(val interface{}) bool {
		return left(val) || right(val)
	}
}

func (o orClause) TransformError(err error) error {
	return err
}

type andClause struct {
	q *queryParser
}

func (a andClause) Parsers() []pars.Parser {
	return []pars.Parser{
		pars.DiscardRight(a.q.fieldParser(), wholeWordParser(pars.StringCI("and"))),
		pars.Recursive(func() pars.Parser {
			return pars.Dispatch(a.q.newAndClause(), a.q.termClause())
		}),
	}
}

func (a andClause) TransformResult(fns []interface{}) interface{} {
	left := fns[0].(queryFunc)
	right := fns[1].(queryFunc)
	return func(val interface{}) bool {
		return left(val) && right(val)
	}
}

func (a andClause) TransformError(err error) error {
	return err
}

func opParser() pars.Parser {
	return pars.Or(
		pars.Transformer(pars.Char('='), func(interface{}) (interface{}, error) {
			return opEquals, nil
		}),
		pars.Transformer(pars.String("<>"), func(interface{}) (interface{}, error) {
			return opNotEquals, nil
		}),
		pars.Transformer(pars.String("<="), func(interface{}) (interface{}, error) {
			return opLessThanOrEquals, nil
		}),
		pars.Transformer(pars.String(">="), func(interface{}) (interface{}, error) {
			return opGreaterThanOrEquals, nil
		}),
		pars.Transformer(pars.Char('<'), func(interface{}) (interface{}, error) {
			return opLessThan, nil
		}),
		pars.Transformer(pars.Char('>'), func(interface{}) (interface{}, error) {
			return opGreaterThan, nil
		}),
		pars.Transformer(likeKeywordParser(), func(interface{}) (interface{}, error) {
			return opLike, nil
		}),
		pars.Transformer(pars.Seq(isKeywordParser(), notKeywordParser()),
			func(interface{}) (interface{}, error) {
				return opNotEquals, nil
			}),
		pars.Transformer(isKeywordParser(), func(interface{}) (interface{}, error) {
			return opEquals, nil
		}),
		pars.Error(errors.New("Operator expected")))
}

func wholeWordParser(p pars.Parser) pars.Parser {
	return pars.SwallowWhitespace(
		pars.Except(
			p.Clone(),
			pars.Seq(
				p.Clone(),
				pars.CharPred(func(r rune) bool {
					return unicode.IsLetter(r) || unicode.IsDigit(r) || r == '_'
				}))))
}

func isKeywordParser() pars.Parser {
	return wholeWordParser(pars.StringCI("is"))
}

func likeKeywordParser() pars.Parser {
	return wholeWordParser(pars.StringCI("like"))
}

func nullKeywordParser() pars.Parser {
	return wholeWordParser(pars.StringCI("null"))
}

func notKeywordParser() pars.Parser {
	return wholeWordParser(pars.StringCI("not"))
}

func identifierParser(name string) pars.Parser {
	return wholeWordParser(pars.String(name))
}

func (q *queryParser) valueParser() pars.Parser {
	clauses := make([]pars.DispatchClause, 0, len(q.fields)+len(q.functions)+4)
	clauses = append(clauses,
		pars.Clause{pars.Int()},
		pars.Clause{pars.Transformer(
			nullKeywordParser(),
			func(v interface{}) (interface{}, error) {
				return nil, nil
			})},
		pars.DescribeClause{
			DispatchClause: pars.StringJoiningClause{
				DispatchClause: pars.Clause{pars.InsteadOf(pars.Char('\''), ""),
					pars.RunesUntil(pars.Char('\'')),
					pars.InsteadOf(pars.Char('\''), ""),
				}},
			Description: "String value"})

	q.fillInFieldAsParameterParser(&clauses)
	q.fillInFunctionParsers(&clauses)

	clauses = append(clauses, pars.Clause{pars.Error(errors.New("Value expected"))})

	return pars.SwallowWhitespace(
		pars.Dispatch(
			clauses...))
}

func (q *queryParser) fillInFunctionParsers(clauses *[]pars.DispatchClause) {
	for _, f := range q.functions {
		clause := pars.DescribeClause{
			DispatchClause: fnParser{f, q},
			Description:    "Function call to " + f.name}
		*clauses = append(*clauses, clause)
	}
}

func (q *queryParser) fillInFieldAsParameterParser(clauses *[]pars.DispatchClause) {
	for _, f := range q.fields {
		*clauses = append(*clauses, pars.Clause{
			pars.InsteadOf(
				identifierParser(f.Name),
				f)})
	}
}

type negated struct {
	pars.DispatchClause
}

func (n negated) Parsers() []pars.Parser {
	parsers := n.DispatchClause.Parsers()
	parsers[0] = pars.DiscardLeft(notKeywordParser(), parsers[0])
	return parsers
}

func (n negated) TransformResult(v []interface{}) interface{} {
	return func(val interface{}) bool {
		return !n.DispatchClause.TransformResult(v).(queryFunc)(val)
	}
}

type lhsExpression struct {
	q *queryParser
}

func (lhs lhsExpression) Parsers() []pars.Parser {
	return []pars.Parser{
		lhs.q.valueParser(),
		opParser(),
		lhs.q.valueParser(),
	}
}

func (lhs lhsExpression) TransformResult(v []interface{}) interface{} {
	op := v[1].(operator)
	compareTo := v[2]
	fn := funcByOP(op, compareTo)
	return func(val interface{}) bool {
		cv := constValue{cval: resolveArgument(v[0], val), current: val}
		return fn(cv)
	}
}

func (lhs lhsExpression) TransformError(err error) error {
	return err
}
