package query

import (
	"bitbucket.org/ragnara/query/iter"
	"math/bits"
	"testing"
)

type intTypes struct {
	i   int
	i8  int8
	i64 int64
	u   uint
	u8  uint8
	u32 uint32
	u64 uint64
}

type intTypesDB []intTypes

func createIntTypesDB() intTypesDB {
	return []intTypes{
		{i: 0, i8: 0, i64: 0, u: 0, u8: 0, u32: 0, u64: 0},
		{i: 1, i8: 1, i64: 1, u: 1, u8: 1, u32: 1, u64: 1},
	}
}

func (db intTypesDB) Fields() []Field {
	return []Field{
		{"i", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).i, false
		}},
		{"i8", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).i8, false
		}},
		{"i64", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).i64, false
		}},
		{"u", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).u, false
		}},
		{"u8", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).u8, false
		}},
		{"u32", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).u32, false
		}},
		{"u64", func(entry interface{}) (interface{}, bool) {
			return entry.(intTypes).u64, false
		}},
	}
}

func (db intTypesDB) Iter() iter.Iter {
	return iter.NewSliceIter(db)
}

func TestSupportedTypes(t *testing.T) {
	fieldNames := []string{"i", "i8", "i64", "u8", "u32"}
	if bits.UintSize == 32 {
		fieldNames = append(fieldNames, "u")
	}

	db := createIntTypesDB()
	proc := NewProcessor(db)

	for _, field := range fieldNames {
		query, _ := proc.Build(field + " = 1")
		results := query.Run()

		if len(results) != 1 || results[0] != db[1] {
			t.Errorf("Expected #%v, but got #%v", db[1], results)
		}
	}
}

func TestUnsupportedTypes(t *testing.T) {
	fieldNames := []string{"u64"}
	if bits.UintSize == 64 {
		fieldNames = append(fieldNames, "u")
	}

	db := createIntTypesDB()
	proc := NewProcessor(db)

	for _, field := range fieldNames {
		query, _ := proc.Build(field + " = 1")

		func() {
			defer func() { recover() }()
			query.Run()
			t.Errorf("type %v did not panic", field)
		}()
	}
}
